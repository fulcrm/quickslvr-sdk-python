.. quickslvr Python SDK documentation master file, created by
   sphinx-quickstart on Tue Apr 21 10:14:03 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to quickslvr Python SDK's documentation!
================================================

Contents:

.. toctree::
   :maxdepth: 2

   quickslvr



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

