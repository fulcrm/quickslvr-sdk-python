"""
Python client library for the quickslvr platform
"""
import json
import urllib
import requests
import time
import logging
import base64

logger = logging.getLogger(__name__)

SCOPES = (
    'profile_read',
    'profile_write',
    'warehouse_read',
    'warehouse_write',
    'job_read',
    'job_write',
    'jobinstance_read',
    'jobinstance_write',
    'job_execute',
    'monitoring_read',
    'monitoring_write'
)


class APIError(Exception):
    def __init__(self, message=None, status_code=None):
        super(APIError, self).__init__(message)
        self.status_code = status_code

    def __str__(self):
        if self.status_code:
            return "%s: %s" % (self.status_code, self.message)
        else:
            return self.message

class APIObjectDoesNotExist(APIError):
    pass


class APIConnectionError(APIError):
    pass


class APIValidationError(APIError):
    status = 400
    data = {}

class APIAuthenticationError(APIError):
    data = {}
    pass


class APIUpstreamConnectionError(APIConnectionError):
    message = 'Did not receive a correct response from the upstream server'


class APICollection(list):
    def __init__(self, data):
        try:
            super(APICollection, self).__init__(data["data"])
            if "pagination" in data:
                self.next_cursor = data["pagination"]["next_cursor"]
            else:
                self.next_cursor = None
        except (KeyError, TypeError):
            raise ValueError("Data did not match the expected format")


class AccessToken(object):
    def __init__(self, token, expires_in=3600, token_type='bearer'):
        if not token:
            raise ValueError("You must provide a token")
        self.token = token
        self.expires_in = expires_in
        self.token_type = token_type

        self.expires = time.time() + expires_in

    def __unicode__(self):
        return self.token

    def __repr__(self):
        return self.token

    @property
    def valid(self):
        return time.time() < self.expires

    @property
    def expired(self):
        return not self.valid


class QuickslvrAPI(object):

    def __init__(self, client_id, client_secret, profile_endpoint=None,
                 profile_attributes_endpoint=False, debug=False,
                 jobs_endpoint=None, auth_endpoint=None, warehouse_endpoint=None,
                 transform_endpoint=None, notifications_endpoint=None, scopes=None):

        if (profile_endpoint and '/' in profile_endpoint)\
                or (jobs_endpoint and '/' in jobs_endpoint)\
                or (auth_endpoint and '/' in auth_endpoint):
            raise ValueError("Endpoints must be hostname only, not URLs")

        self.debug = debug

        self.version = 1  # not configurable, placeholder for future versions
        self.profile_endpoint = profile_endpoint
        self.profile_attributes_endpoint = profile_attributes_endpoint
        self.jobs_endpoint = jobs_endpoint
        self.auth_endpoint = auth_endpoint
        self.warehouse_endpoint = warehouse_endpoint
        self.transform_endpoint = transform_endpoint
        self.notifications_endpoint = notifications_endpoint

        self._access_token = None
        self.client_id = client_id
        self.client_secret = client_secret

        self.scopes = scopes or SCOPES  # ()

    def _handle_error_response(self, response):

        if self.debug:
            logger.error("Received API error %s" % response.status_code)
            logger.error(response.content)

        try:
            data = response.json()
        except ValueError:  # response is not valid json
            raise APIError(response.content, response.status_code)

        try:
            if data["error"]["code"] == "validation_error":
                raise APIValidationError(data['error']['detail'])
        finally:
            raise APIError(response.content, response.status_code)

    @property
    def access_token(self):
        if self._access_token and self._access_token.valid:
            return self._access_token
        data = self.get_auth_token(self.client_id, self.client_secret, self.scopes)
        self._access_token = AccessToken(
            token=data['access_token'],
            expires_in=data['expires_in'],
            token_type=data['token_type']
        )
        return self._access_token

    def _request(self, path, method="GET", params=None, data=None, endpoint=None, custom_headers=None):

        if not endpoint:
            raise ValueError('Endpoint has not been specified for this request')

        headers = {
            'X-Requested-With': 'XMLHttpRequest',
            'Authorization': 'Bearer %s' % self.access_token.token
        }

        if custom_headers:
            headers.update(custom_headers)

        url = 'http://' + endpoint + '/v1/' + path

        if params:
            params = {k: v for k, v in params.items() if v}

        logger.debug("%s %s%s%s" % (method, url, '?' if params else '', urllib.urlencode(params) if params else ''))
        try:
            start = time.time()
            response = requests.request(method,
                                        url,
                                        params=params,
                                        data=data,
                                        headers=headers)
            logger.debug("Response %s (%s s)" % (response.status_code, (time.time() - start)))

        except requests.HTTPError as e:
            response = json.loads(e.read())
            raise APIError(response)
        except requests.ConnectionError:
            raise APIConnectionError("Could not connect to %s" % endpoint)

        if response.status_code == 404:
            raise APIObjectDoesNotExist

        # Handle upstream connection errors
        if response.status_code in (502, 504):
            raise APIUpstreamConnectionError(status_code=response.status_code)

        if not response.ok:
            self._handle_error_response(response)

        return response

    def _data_from_result(self, result):
        try:
            return result["data"]
        except KeyError:
            raise APIError("Response did not match the expected format")

    def _get_object(self, collection_name, object_id, endpoint=None):
        response = self._request(unicode(collection_name) + '/' + unicode(object_id), endpoint=endpoint)
        return self._data_from_result(response.json())

    def _delete_object(self, collection_name, object_id, endpoint=None):
        response = self._request(unicode(collection_name) + '/' + unicode(object_id), method="DELETE", endpoint=endpoint)
        if not response.ok:
            raise APIError(response.json())
        else:
            return True

    def _create_object(self, collection_name, **data):
        endpoint = data.pop("endpoint", None)
        response = self._request(unicode(collection_name), method="POST", data=data, endpoint=endpoint)
        if response.status_code == 201:
            return self._data_from_result(response.json())
        elif response.status_code == 400:
            raise APIValidationError(response.json()["detail"])
        else:
            raise APIError(response.json())

    def _update_object(self, collection_name, id, **data):
        endpoint = data.pop("endpoint", None)
        custom_headers = data.pop("custom_headers", None)

        if custom_headers == {'Content-Type': 'application/json'}:
            data = json.dumps(data)

        response = self._request("%s/%s" % (unicode(collection_name), unicode(id)),
                                 method="PUT",
                                 data=data,
                                 endpoint=endpoint,
                                 custom_headers=custom_headers)
        if response.ok:
            return self._data_from_result(response.json())
        else:
            raise APIError(response.json())

    def _get_collection(self, collection_name, cursor=None, count=None, endpoint=None):
        response = self._request(collection_name, endpoint=endpoint, params={
            'cursor': cursor,
            'count': count
        })
        if not response.ok:
            raise APIError(status_code=response.status_code)
        return response.json()

    def submit(self, event_type, profile, payload={}):
        event = {
            'profile': profile,
            'event_type': event_type,
            'payload': payload
        }

        resp = self._request("submit", endpoint=self.profile_endpoint, method="GET", params={'data': base64.encodestring(json.dumps(event))})

        if resp.ok:
            return resp.json()
        else:
            raise APIError(status=resp.status_code, content=resp.content)

    def get_profiles(self, cursor=None, count=None):
        """
        Get profile collection

        :param str cursor: cursor to start at
        :param int count: number of results to return
        :returns: list of results
        :rtype: list
        """
        return self._get_collection('profiles', cursor, count, endpoint=self.profile_endpoint)

    def get_profile(self, id):
        """
        Get a single profile by id or alias

        :param str id: the id or alias in the form keytype:value
        :returns: the profile retrieved
        :rtype: dict
        :raises: APIObjectDoesNotExist if the profile is not found
        """
        return self._get_object('profiles', id, endpoint=self.profile_endpoint)

    def delete_profile(self, id):
        """
        Delete a profile and all keys, events and segment memberships

        :param str id: The id or alias of the profile to delete
        :returns: True if the profile was successfully deleted
        :rtype: bool
        """
        return self._delete_object('profiles', id, endpoint=self.profile_endpoint)

    def get_profile_events(self, profile_id, cursor=None, count=None, order='asc'):
        """
        Get events for a given profile

        :param str profile_id: the id or alias of the profile to query, in the form keytype:value
        :param str cursor: the cursor to begin results at
        :param int count: number of results to return
        :returns: list of events
        :rtype: list
        """
        response = self._request('profiles/%s/events' % profile_id, params={
            'cursor': cursor,
            'count': count,
            'order': order
        }, endpoint=self.profile_endpoint)

        return APICollection(response.json())

    def get_profile_event(self, profile_id, event_id):
        """
        Get a single event

        :param str profile_id: the id or alias of the profile to which the event belongs, in the form keytype:value
        :param int event_id: the numeric id of the event to retrieve
        :returns: the event
        :rtype: dict
        """
        response = self._request('profiles/%s/events/%s' % (profile_id, event_id), endpoint=self.profile_endpoint)
        return self._data_from_result(response.json())

    def delete_profile_event(self, profile_id, event_id):
        """
        Delete a single event from a profile

        :param str profile_id: the id or alias of the profile to which the event belongs, in the form keytype:value
        :param int event_id: the numeric id of the event to delete
        :return: whether the event was successfully deleted
        :rtype: bool
        """
        response = self._request('profiles/%s/events/%s' % (profile_id, event_id), method="DELETE", endpoint=self.profile_endpoint)
        if not response.ok:
            raise APIError(response.json())

    def profile_merge(self, parent_id, child_id):
        """
        Merge two profiles with the given ids

        :param str parent_id:
        :param str child_id:
        :returns: whether the profiles were successfully merged
        :rtype: bool
        """
        response = self._request('profiles/%s/merge' % parent_id, method="POST", data={
            "id": child_id
        }, endpoint=self.profile_endpoint)
        if not response.ok:
            raise APIError("Server response did not match the expected format (202)")
        return True

    def get_event_types(self, cursor=None, count=None):
        """
        Get list of available event types

        :param str cursor: cursor to start at
        :param int count: number of results to return
        :returns: list of event types
        :rtype: list
        """
        return self._get_collection('event_types', cursor, count, endpoint=self.profile_endpoint)

    def get_event_type(self, id):
        """
        Get a single event type

        :param str id: the alphanumeric short name of the event type to retrieve
        :returns: the event type
        :rtype: dict
        :raises APIObjectDoesNotExist: if the requested object is not found
        """
        return self._get_object('event_types', id, endpoint=self.profile_endpoint)

    def create_event_type(self, name, display_name, schema, weight):
        """
        Create a new event type

        :param str name: Machine name of the new event type - letters, numbers and underscores only
        :param str display_name: Display name of the new event type - free text
        :param dict schema: dictionary describing the JSON schema of the new event type
        :param int weight: numeric weight for the new event type
        :returns: dictionary of the newly created event type
        :rtype dict:
        """
        response = self._request('event_types', method="POST", data={
            'name': name,
            'display_name': display_name,
            'schema': json.dumps(schema) if isinstance(schema, dict) else schema,
            'weight': weight
        }, endpoint=self.profile_endpoint)
        if response.status_code == 201:
            return self._data_from_result(response.json())
        else:
            raise APIError(response.content)

    def update_event_type(self, id, **kwargs):
        """
        Change the parameters of an existing event type

        :param str id: the short name of the event type to change
        :param dict kwargs: dictionary of parameters to update
        :returns: dictionary describing the updated event type
        :rtype: dict
        """

        if u'schema' in kwargs and isinstance(kwargs[u'schema'], dict):
            kwargs[u'schema'] = json.dumps(kwargs[u'schema'])

        return self._update_object('event_types', id, endpoint=self.profile_endpoint, **kwargs)

    def delete_event_type(self, id):
        """
        Delete an event type

        :param str id: short name of the event type to delete
        :returns: whether the event type was successfully deleted
        :rtype: bool
        """
        return self._delete_object('event_types', id, endpoint=self.profile_endpoint)

    def get_attribute_types(self, cursor=None, count=None):
        """
        Get list of available attribute types

        :param str cursor: cursor to start at
        :param int count: number of results to return
        :returns: list of attribute types
        :rtype: list
        """
        return self._get_collection('attribute_types', cursor, count, endpoint=self.profile_attributes_endpoint)

    def get_attribute_type(self, name):
        """
        Get a single attribute type

        :param str name: the alphanumeric short name of the attribute type to retrieve
        :returns: the attribute type
        :rtype: dict
        :raises APIObjectDoesNotExist: if the requested object is not found
        """
        return self._get_object('attribute_types', name, endpoint=self.profile_attributes_endpoint)


    def create_attribute_type(self, name, display_name, encrypted, remote, schema):
        """
        Create a new attribute type

        :param str name: Machine name of the new attribute type - letters, numbers and underscores only
        :param str display_name: Display name of the new attribute type - free text
        :param bool encrypted: whether to automatically encrypt this value
        :param bool remote: whether to automatically reference & dereference this value in the remote K/V store
        :param dict schema: dictionary describing the JSON schema of the new attribute type
        :returns: dictionary of the newly created attribute type
        :rtype dict:
        """
        response = self._request('attribute_types', method="POST", data=json.dumps({
            'name': name,
            'display_name': display_name,
            'encrypted': encrypted,
            'remote': remote,
            'schema': json.dumps(schema) if schema != "{}" else schema
        }), endpoint=self.profile_attributes_endpoint,
            custom_headers={'Content-Type': 'application/json'})

        if response.status_code == 201:
            return response.status_code == 201, response.json()
        else:
            self._handle_error_response(resp)

    def update_attribute_type(self, name, **kwargs):
        """
        Change the parameters of an existing attribute type

        :param str name: the short name of the attribute type to change
        :param dict kwargs: dictionary of parameters to update
        :returns: dictionary describing the updated attribute type
        :rtype: dict
        """

        if u'schema' in kwargs and isinstance(kwargs[u'schema'], dict):
            kwargs[u'schema'] = json.dumps(kwargs[u'schema'])

        return self._update_object(
            'attribute_types',
            name,
            endpoint=self.profile_attributes_endpoint,
            custom_headers={'Content-Type': 'application/json'},
            **kwargs)

    def delete_attribute_type(self, name):
        """
        Delete an attribute type

        :param str name: short name of the attribute type to delete
        :returns: whether the attribute type was successfully deleted
        :rtype: bool
        """
        return self._delete_object('attribute_types', name, endpoint=self.profile_attributes_endpoint)

    def get_profile_key_types(self, cursor=None, count=None):
        """
        Get profile key types

        :param str cursor: cursor to start at
        :param int count: the number of results to return
        :returns: list of results
        :rtype: list
        """
        return self._get_collection('profile_key_types', cursor, count, endpoint=self.profile_endpoint)

    def get_profile_key_type(self, id):
        """
        Get a single profile key type by id

        :param str id: the short name of the profile key type to return
        :returns: dictionary describing the profile key type
        :rtype: dict
        """
        return self._get_object('profile_key_types', id, endpoint=self.profile_endpoint)

    def create_profile_key_type(self, name):
        """
        Create a profile key type

        :param str name:
        :returns: dictionary describing the newly created profile key type
        :rtype: dict
        """
        return self._create_object('profile_key_types', name=name, endpoint=self.profile_endpoint)

    def update_profile_key_type(self, id, **kwargs):
        """
        Update a profile key type by short name

        :param str id: short name of the profile key type to update
        :param dict kwargs: dictionary of paramters
        :returns: dictionary describing the newly updated profile key
        :rtype: dict
        """
        return self._update_object('profile_key_types', id, endpoint=self.profile_endpoint, **kwargs)

    def delete_profile_key_type(self, id):
        """
        Delete a profile key type by short name

        :param str id: short name of the profile key type to delete
        :returns: whether the profile key was successfully deleted
        :rtype: bool
        """
        return self._delete_object('profile_key_types', id, endpoint=self.profile_endpoint)

    def get_segments(self, cursor=None, count=None):
        """
        Retrieve a list of segments

        :param str cursor: cursor to start at
        :param int count: number of results to return
        :returns: list of segments retrieved
        :rtype: list
        """
        return self._get_collection('segments', cursor, count, endpoint=self.profile_endpoint)

    def get_segment(self, id):
        """
        Retrieve a segment by short name

        :param str id: the short name of the segment to retrieve
        :returns: dictionary describing the retrieved segment
        :rtype: dict
        """
        return self._get_object('segments', id, endpoint=self.profile_endpoint)

    def create_segment(self, name, display_name=None):
        """
        Create a new segment

        :param str name: name of the segment to create -- letters, numbers and underscores only
        :returns: dictionary describing the newly created segment
        :rtype dict:
        """
        display_name = display_name or name
        return self._create_object('segments', name=name, display_name=display_name, endpoint=self.profile_endpoint)

    def delete_segment(self, id):
        """
        Delete a segment by name

        :param str id: short name of the segment to delete
        :return: whether the segment was successfully deleted
        :rtype: dict
        """
        return self._delete_object('segments', id, endpoint=self.profile_endpoint)

    # Web hooks

    def get_webhooks(self, cursor=None, count=None):
        """
        Get all webhooks

        :param str cursor: cursor to start at
        :param int count: number of results to return
        :return: list of webhooks retrieved
        :rtype: list
        """
        return self._get_collection('hooks', endpoint=self.profile_endpoint)

    def get_webhook(self, name):
        """
        Get a single web hook

        :param str name: short name of the web hook to retrieve
        :returns: dictionary describing the webhook retrieved
        :rtype: dict
        """

        return self._get_object("hooks", name, endpoint=self.profile_endpoint)

    def create_webhook(self, name, display_name, url, event_types):
        """
        Create a new web hook

        :param str name: short name of the web hook, letters, numbers and underscores only
        :param str display_name: display name for the web hook
        :param str url: URL to post to
        :param list event_types: list of event type short names to post to
        :returns: dictionary describing the newly created webhook
        :rtype: dict
        """

        return self._create_object("hooks",
                            name=name,
                            display_name=display_name,
                            url=url,
                            event_types=event_types,
                            endpoint=self.profile_endpoint)

    def update_webhook(self, name, **kwargs):
        """
        Update an existing web hook

        :param str name: short name of the web hook to update
        :param dict kwargs: parameters to update
        :returns: dictionary describing the updated webhook
        :rtype: dict
        """

        return self._update_object("hooks", name, endpoint=self.profile_endpoint, **kwargs)

    def delete_webhook(self, name):
        """
        Delete an existing web hook

        :param str name: the short name of the web hook to delete
        :returns: whether the webhook was successfully deleted
        :rtype: bool
        """

        self._delete_object("hooks", name, endpoint=self.profile_endpoint)

    # Enrichment hooks

    def get_enrichment_hook_actions(self, cursor=None, count=None):
        """
        Get all enrichment hook actions

        :param str cursor: cursor to start at
        :param int count: number of results to return
        :return:
        """
        return self._get_collection('enrichment_hook_actions', endpoint=self.profile_endpoint)


    def get_enrichment_hook_action(self, name):
        """
        Get a single enrichment hook action

        :param str name: short name of the enrichment hook action to retrieve
        :return dict:
        """
        return self._get_object("enrichment_hook_actions", name, endpoint=self.profile_endpoint)


    def get_enrichment_hooks(self, cursor=None, count=None):
        """
        Get all enrichment hooks

        :param str cursor: cursor to start at
        :param int count: number of results to return
        :return: list of retrieved enrichment hooks retrieved
        :rtype: list
        """
        return self._get_collection('enrichment_hooks', cursor, count, endpoint=self.profile_endpoint)

    def get_enrichment_hook(self, name):
        """
        Get a single enrichment hook

        :param str name: short name of the enrichment hook to retrieve
        :returns: dictionary describing the retrieved enrichment hook
        :rtype: dict
        """
        return self._get_object("enrichment_hooks", name, endpoint=self.profile_endpoint)

    def create_enrichment_hook(self, name, display_name, url, event_types,
                               order, is_active, action, condition, segment,
                               counter, counter_incr):
        """
        Create a new enrichment hook

        :param str name: short name of the enrichment hook, letters, numbers and underscores only
        :param str display_name: display name for the enrichment hook
        :param str url: URL to post to
        :param list event_types: list of event type short names to post to
        :param int order: order at which the enrichment hook should be applied
        :param bool is_active: select to enable the hook
        :param str action: action to be performed by the hook
        :param str condition: execute the hook if this condition is met
        :param str segment: add profile to this segment
        :param str counter: optional counter name, required in the 'Increment counter' action
        :param int counter_incr: optional, increase counter by this value
        :return dict:
        """
        return self._create_object("enrichment_hooks",
                                   name=name,
                                   display_name=display_name,
                                   url=url,
                                   event_types=event_types,
                                   order=order,
                                   is_active=is_active,
                                   action=action,
                                   condition=condition,
                                   segment=segment,
                                   counter=counter,
                                   counter_incr=counter_incr,
                                   endpoint=self.profile_endpoint)

    def update_enrichment_hook(self, name, **kwargs):
        """
        Update an existing enrichment hook

        :param str name: short name of the enrichment hook to update
        :param dict kwargs: parameters to update
        :returns: dictionary describing the updated enrichment hook
        :rtype: dict
        """
        return self._update_object("enrichment_hooks", name, endpoint=self.profile_endpoint, **kwargs)

    def delete_enrichment_hook(self, name):
        """
        Delete an existing enrichment hook

        :param str name: the short name of the enrichment hook to delete
        :return: whether the enrichment hook was successfully deleted
        :rtype: bool
        """

        self._delete_object("enrichment_hooks", name, endpoint=self.profile_endpoint)

    # Scheduled jobs

    def get_scheduled_jobs(self, cursor=None, count=None):
        """
        Retrieve a list of scheduled jobs

        :param str cursor: cursor to begin results at
        :param int count: number of results to return
        :returns list: list of scheduled jobs retrieved
        :rtype: list
        """
        return self._get_collection('scheduled_jobs', endpoint=self.jobs_endpoint)

    def get_scheduled_job(self, id):
        """

        :param int id:  id of the scheduled job to retrieve
        :returns: dictionary describing the retrieved job
        :rtype: dict
        """
        data = self._get_object('scheduled_jobs', id, endpoint=self.jobs_endpoint)
        return data

    def create_scheduled_job(self, **kwargs):
        """
        Create a new scheduled job

        :param str name: short name for the job
        :param str job_type: type of job to create. One of `python`, `shell`, `talend`
        :param str input_type: one of `file` or `script`
        :param str file: if `input_type` is `file`, this is the url of the file to use
        :param str script: if `input_type` is `script`, this is the content of the script to run
        :param bool cron_enabled: whether the cron schedule should be enabled for this job
        :param str cron_schedule: schedule for this job, in standard cron format
        :returns: dictionary describing the newly created job
        :rtype: dict
        """
        return self._create_object('scheduled_jobs', endpoint=self.jobs_endpoint, **kwargs)

    def update_scheduled_job(self, id, **kwargs):
        """

        Update scheduled job with the given identifier

        :param int id:  id of the job to updated
        :param dict kwargs:  parameters to change
        :returns: dictionary describing the newly updated job
        :rtype: dict
        """
        return self._update_object('scheduled_jobs', id, endpoint=self.jobs_endpoint, **kwargs)

    def delete_scheduled_job(self, id):
        """
        Delete a scheduled job with the given identifier

        :param int id: id of the job to delete
        :returns: whether the job was successfully deleted
        :rtype: bool
        """
        return self._delete_object('scheduled_jobs', id, endpoint=self.jobs_endpoint)

    def get_scheduled_job_script(self, id):
        """
        Return the script for a given scheduled job

        :param int id: id of the scheduled job required
        :returns: content of the script for that job
        :rtype: string
        """
        resp = self._request('scheduled_jobs/%s/script' % unicode(id), endpoint=self.jobs_endpoint)
        return resp.content

    def get_scheduled_job_instances(self, id, cursor=None, count=None):
        """
        Retrieve instances for job with a given id

        :param int id:  id of the job to get instances for
        :param str cursor:  cursor to start results at
        :param int count:  number of results to return
        :returns: list of instances for job with the given id
        :rtype: list
        :raises: APIObjectDoesNotExist if no job exists with the given id
        """
        response = self._request('scheduled_jobs/%s/instances' % id, endpoint=self.jobs_endpoint, params={
            'cursor': cursor,
            'count': count
        })
        if not response.ok:
            raise APIError(response.content)
        return self._data_from_result(response.json())

    def get_scheduled_job_instance(self, job_id, uuid):
        """
        Retrieve a single job instance

        :param int job_id: id of the job
        :param str uuid: uuid of the instance to retrieve
        :returns: dictionary describing the retrieved instance
        :rtype: dict
        """
        response = self._request('scheduled_jobs/%s/instances/%s' % (job_id, uuid), endpoint=self.jobs_endpoint)
        if not response.ok:
            raise APIError(response.content)
        return self._data_from_result(response.json())

    def update_scheduled_job_instance(self, uuid, append=False, **kwargs):
        """

        :param str uuid: UUID of the job instance to update
        :param bool append: whether to append output lines or replace them
        :param dict kwargs: parameters to update
        :returns: dict describing the newly updated job instance
        :rtype: dict
        """
        response = self._request('job_instances/%s' % uuid,
                                endpoint=self.jobs_endpoint,
                                data=kwargs,
                                params={'append': append},
                                method='POST')
        if not response.ok:
            raise APIError(response.content)
        return True

    def delete_scheduled_job_instance(self, job_id, uuid):
        """
        Delete a job instance with the given id

        :param int job_id: id of the instances parent job
        :param str uuid: uuid of the instance to delete
        :returns: whether the instance was successfully deleted
        :rtype: bool
        """
        response = self._request('scheduled_jobs/%s/instances/%s' % (job_id, uuid),
                                method='DELETE', endpoint=self.jobs_endpoint)
        if not response.ok:
            raise APIError(response.content)
        return True

    def execute_job(self, id):
        """
        Run a job with the given id

        :param int id: id of the job to run
        """
        resp = self._request('scheduled_jobs/%s/execute' % id,
                            method='POST',
                            endpoint=self.jobs_endpoint)
        if not resp.ok:
            raise APIError()

    # Auth

    def get_auth_clients(self, cursor=None, count=None):
        """
        Retrieve a list of auth clients

        :param str cursor: cursor to start results at
        :param int count: number of results to return
        :returns; list of dictionaries describing the returned auth clients
        :rtype: list(dict)
        """
        return self._get_collection('clients', cursor, count, endpoint=self.auth_endpoint)

    def create_auth_client(self, display_name):
        """
        Create a new auth client

        :param str display_name: display name for the client
        :returns: dictionary describing the newly created auth client
        :rtype: dict
        """
        return self._create_object('clients', display_name=display_name, endpoint=self.auth_endpoint)

    def get_auth_client(self, id):
        """
        Retrieve a single auth client by id

        :param int id: id of the auth client to retrieve
        :returns: dictionary describing the retrieved client
        :raises: APIObjectDoesNotExist if no auth client exists with the given id
        """
        return self._get_object('clients', id, endpoint=self.auth_endpoint)

    def update_auth_client(self, id, display_name):
        """
        Update an auth client with the given id

        :param int id: id of the client to update
        :param str display_name: new display name to assign this client
        :returns: dictionary describing the newly updated client
        :rtype: dict
        """
        return self._update_object('clients', id, display_name=display_name, endpoint=self.auth_endpoint)

    def delete_auth_client(self, id):
        """
        Delete an auth client with the given id

        :param int id: id of the client to delete
        :returns: whether the client was successfully deleted
        :rtype: bool
        """
        return self._delete_object('clients', id, endpoint=self.auth_endpoint)

    def reset_auth_client(self, id):
        """
        Reset the tokens for an auth client

        :param int id: The numeric id of the auth client to reset
        :return:
        """
        resp = self._request(
            'clients/%s/reset' % id,
            method="POST",
            endpoint=self.auth_endpoint
        )
        if not resp.ok:
            return self._handle_error_response(resp)

        return resp.json()["data"]

    def get_auth_token(self, client_id, client_secret, scopes):
        """
        Obtain an access token for a client

        :param int client_id: the numeric id of the client
        :param str client_secret: the client's secret
        :param list scopes: the scopes to obtain access for
        :returns dict: the newly created token
        """
        if not self.auth_endpoint:
            raise ValueError('No auth endpoint has been specified!')

        logger.debug("POST http://%s/v1/token" % self.auth_endpoint)
        start = time.time()
        resp = requests.post(
            'http://%s/v1/token' % self.auth_endpoint,
            auth=(client_id, client_secret),
            data={
                'scope': ' '.join(scopes),
                'grant_type': 'client_credentials'
            },
            headers={'X-Requested-With': 'XMLHttpRequest'}
        )

        logger.debug("Response %s (%s s)" % (resp.status_code, (time.time() - start)))

        if not resp.ok:
            return self._handle_error_response(resp)

        return resp.json()

    # Warehouse

    def get_entity_types(self):
        """
        Retrieve a list of entity types

        :returns: list of entity types retrieved
        :rtype: list(dict)
        """

        return self._get_collection('entity_types', endpoint=self.warehouse_endpoint)

    def get_entity_type(self, name):
        """
        Retrieve an entity type by name

        :param str name:  name of the entity type to retrieve
        :returns: dictionary describing the retrieved entity type
        """

        return self._get_object('entity_types', name, endpoint=self.warehouse_endpoint)

    def create_entity_type(self, name, schema, has_profile=False):
        """
        Create an entity type
        :param str name: short name of the entity type
        :param dict schema: JSON schema to use for entity instances
        :param bool has_profile:
        :returns dict:
        :raises APIError: if the server returned an unexpected response
        """

        response = self._request('entity_types', method="POST", data=json.dumps({
            'name': name,
            'schema': schema,
            'has_profile': has_profile
        }), endpoint=self.warehouse_endpoint)
        if response.status_code == 201:
            return self._data_from_result(response.json())
        else:
            self._handle_error_response(response)

    def update_entity_type(self, name, **kwargs):
        """
        Update a warehouse entity type

        :param str name: name of the entity type to update
        :param dict kwargs: parameters to update
        :returns: dictionary describing the newly updated entity type
        :rtype: dict
        """

        response = self._request('entity_types/%s' % name, method="PUT", data=json.dumps(kwargs), endpoint=self.warehouse_endpoint)

        if response.ok:
            return self._data_from_result(response.json())
        else:
            self._handle_error_response(response)

    def delete_entity_type(self, name):
        """
        Delete a warehouse entity type by name

        :param str name: name of the entity type to delete
        :returns: whether the entity type was successfully deleted
        :rtype: bool
        """
        return self._delete_object('entity_types', name, endpoint=self.warehouse_endpoint)

    def get_entities(self, entity_type=None, profile=None, count=10, start=0):
        """
        Get entities for a given entity type

        :param str entity_type: the entity type to retrieve rows for
        :param str profile: a profile key to retrieve rows for
        :param int count: number of rows to retrieve
        :param int start: offset to begin results at
        :returns: list of results
        :rtype: list
        """

        params = {
            'count': count,
            'start': start
        }

        if entity_type:
            params['type'] = entity_type

        if profile:
            params['profile'] = profile

        resp = self._request('entities'.format(entity_type), endpoint=self.warehouse_endpoint, params=params)

        if resp.ok:
            return resp.json()
        else:
            self._handle_error_response(resp)

    def get_entity(self, entity_type, id):
        """
        Get a single entity by type and uuid

        :param entity_type: type of entity to get
        :param uuid: uuid of the entity
        :returns:  the entity
        :rtype: dict
        :raises APIObjectDoesNotExist: if the requested entity does not exist
        """

        resp = self._request('entities/{}'.format(id), endpoint=self.warehouse_endpoint)

        if resp.ok:
            return self._data_from_result(resp.json())
        else:
            self._handle_error_response(resp)

    def create_entity(self, entity_type, profile, payload, upsert=False):
        """
        Create a new entity

        :param str entity_type: type of entity to create
        :param str profile: profile key of the profile this entity should be attached to
        :param dict payload: json object of data for this entity
        :param bool upsert: whether to perform an upsert with the given parameters
        :returns: created, data
        :rtype: tuple
        """

        resp = self._request('entities', method="POST", params={'upsert': upsert}, data=json.dumps({
            "type": entity_type,
            "profile": profile,
            "payload": payload
        }), endpoint=self.warehouse_endpoint)

        if resp.status_code in (200, 201):
            return resp.status_code == 201, resp.json()
        else:
            self._handle_error_response(resp)

    def upsert_entity(self, *args, **kwargs):
        return self.create_entity(upsert=True, *args, **kwargs)

    def update_entity(self, entity_type, id, **kwargs):
        """
        Update an existing entity

        :param str entity_type: type of entity to update
        :param str profile: profile to which this entity should belong
        :param dict payload: updated payload for this entity
        :return: dictionary describing the newly updated entity
        :rtype: dict
        """

        resp = self._request('entities/{}'.format(id), method="PUT", data=json.dumps(kwargs), endpoint=self.warehouse_endpoint)

        if resp.ok:
            return self._data_from_result(resp.json())
        else:
            self._handle_error_response(resp)

    def delete_entity(self, entity_type, id):
        """
        Delete an entity

        :param str entity_type: type of the entity to delete
        :param int id: id of the entity to delete
        :returns: whether the entity was successfully deleted
        :rtype: bool
        """
        return self._delete_object('entities', id, endpoint=self.warehouse_endpoint)

    def get_entity_history(self, entity_type, id):
        """
        Get revision history for an entity

        :param str entity_type: type of the to retrieve history for
        :param int id: id of the entity to retrieve history for
        :returns: list of previous versions of this entity
        :rtype: list
        """

        return self._get_collection('entities/{}/history'.format(id), endpoint=self.warehouse_endpoint)

    def get_transform_rules(self, event_type=None):
        """
        Get transform rules, optionally for a given event type

        :param str event_type: event type to get rules for
        :returns: list of transform rules
        :rtype: list
        """

        response = self._request(
            'rules',
            endpoint=self.transform_endpoint,
            params={'event_type': event_type} if event_type else {})
        if not response.ok:
            raise APIError(status_code=response.status_code)
        return response.json()

    def get_transform_rule(self, name):
        """
        Get a single transform rule by name

        :param str name: name of transform rule to get
        :returns: dictionary describing the retrieved rule
        """

        return self._get_object('rules', name, endpoint=self.transform_endpoint)

    def create_transform_rule(self, name, event_type, entity_type, entity_transform,
                              event_transform=None, active=True, order=100):
        """
        Create a new transform rule

        :param str name: unique identifier for the rule
        :param str event_type: event type to which this rule should apply
        :param str entity_type: entity type which this transform rule will create
        :param str entity_transform: jq transformation describing the new entity payload
        :param str event_transform: optional jq transformation describing the replacement event payload
        :param bool active: whether this rule should be applied
        :param int order: relative order in which this rule should be applied
        :return:
        """

        data = {
            'name': name,
            'event_type': event_type,
            'entity_type': entity_type,
            'entity_transform': entity_transform,
            'active': active,
            'order': order
        }

        if event_transform:
            data['event_transform'] = event_transform

        response = self._request('rules', method='POST', data=json.dumps(data), endpoint=self.transform_endpoint)

        if response.ok:
            return response.json()
        else:
            raise APIError(status_code=response.status_code)

    def delete_transform_rule(self, name):
        """
        Delete transform rule with the given identifier

        :param name: name of the transform rule to delete
        :return:
        """

        response = self._delete_object('rules', name, endpoint=self.transform_endpoint)

    def update_transform_rule(self, name, **kwargs):
        """
        Update transform rule with the given identifier

        :param str name: name of rule to update
        :param dict kwargs: parameters to change
        :returns dict: updated rule
        """

        response = self._request(
            'rules/{}'.format(name),
            method='PUT',
            data=json.dumps(kwargs),
            endpoint=self.transform_endpoint)

        if response.status_code == 201:
            return response.json()
        else:
            raise APIError(status_code=response.status_code)

    # Notifications

    def get_notification_list(self, count=None):
        """
        Get a list of all notifications

        :return:
        """
        return self._get_collection('notifications', count=count, endpoint=self.notifications_endpoint)

    def get_notification(self, id):
        return self._get_object('notifications', id, endpoint=self.notifications_endpoint)

    def delete_notification(self, id):
        return self._delete_object('notifications', id, endpoint=self.notifications_endpoint)

    def get_notification_count(self):
        """
        Get a count of new notifications for each notification type

        :returns dict: dictionary of counts for each type
        """

        response = self._request('count', endpoint=self.notifications_endpoint)

        if response.status_code == 200:
            return response.json()['data']
        else:
            raise APIError(status_code=response.status_code)

    def create_notification(self, message, level=None, detail=None):
        """
        Create a new notification

        :param str message: notification message
        :param str level: message level, one of debug, info, notice, warning, error, critical
        :param str detail: more detail for the message
        """

        resp = self._request('notifications', method="POST", data=json.dumps({
            "message": message,
            "level": level,
            "detail": detail
        }), endpoint=self.monitoring_endpoint)

        if resp.status_code in (200, 201):
            return resp.status_code == 201, resp.json()
        else:
            self._handle_error_response(resp)

    # Counters

    def get_counters(self, cursor=None, count=None):
        """
        Retrieve a list of counters

        :param str cursor: cursor to start at
        :param int count: number of results to return
        :returns: list of counters retrieved
        :rtype: list
        """
        return self._get_collection('counters', cursor, count, endpoint=self.profile_endpoint)

    def get_counter(self, id):
        """
        Retrieve a counter by short name

        :param str id: the short name of the counter to retrieve
        :returns: dictionary describing the retrieved counter
        :rtype: dict
        """
        return self._get_object('counters', id, endpoint=self.profile_endpoint)

    def create_counter(self, name, display_name='', radioactive=False, half_life=None):
        """
        Create a new counter

        :param str name: name of the counter to create -- letters, numbers and underscores only
        :returns: dictionary describing the newly created counter
        :rtype dict:
        """
        return self._create_object(
            'counters',
            name=name,
            radioactive=radioactive,
            half_life=half_life,
            endpoint=self.profile_endpoint)

    def update_counter(self, id, **kwargs):
        """
        Update an existing counter

        :param str id: short name of the counter to update
        :param dict kwargs: parameters to update
        :returns: dictionary describing the updated counter
        :rtype: dict
        """
        return self._update_object("counters", id, endpoint=self.profile_endpoint, **kwargs)

    def delete_counter(self, id):
        """
        Delete a counter by name

        :param str id: short name of the counter to delete
        :return: whether the counter was successfully deleted
        :rtype: dict
        """
        return self._delete_object('counters', id, endpoint=self.profile_endpoint)
