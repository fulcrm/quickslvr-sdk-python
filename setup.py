from setuptools import setup

setup(name='quickslvr-sdk',
      version='0.1',
      description='Python SDK for quickslvr',
      url='https://bitbucket.org/fulcrm/quickslvr-sdk-python',
      author='Adam Gray',
      author_email='adam.gray@havashelia.com',
      license='Proprietary',
      packages=['quickslvr'],
      install_requires=[
          'requests'
      ],
      zip_safe=False
)