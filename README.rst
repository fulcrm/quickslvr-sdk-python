====================
quickslvr Python SDK
====================

Copyright Havas EHS Ltd 2015

Currently a minimal wrapper around the quickslvr Profile Engine REST API.  More
features, functionality and fun to come.

For more information see: http://www.havasquickslvr.com 

quickslvr is a trademark of Havas EHS Limited.

------------
Installation
------------
::

    $ python setup.py install

-----
Usage
-----

 >>> from quickslvr import QuickslvrAPI
 >>> api = QuickslvrAPI('https://your-endpoint.quickslvr.com')

 >>> # Get data for profile cookie:12345
 >>> api.get_profile('cookie:12345')

 >>> # List events for profile cookie:12345
 >>> api.get_profile_events('cookie:12345')

 >>> # Create page_view event type
 >>> api.create_event_type(name='page_view', display_name='Page view', weight=100)

 >>> # Update change name and weight of page_view event type
 >>> api.update_event_type('page_view', display_name='Page View', weight=120)